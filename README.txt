This module restricts login access by language.
For the user to login successfully, its language must match the default
language of the site.

You can also use the permission "Login with any language".

This module was based on Domain Access.
